# lab0-setup

Setup your OCI environment for the hands-on workshop

## Provision your OCI Tenant

There are 2 possibilities for this workshop:
  * OCI Free Tiers: 
  * Oracle OCI Workshop Tenant

### 1. OCI Free Tiers [https://signup.cloud.oracle.com](https://signup.cloud.oracle.com)

This option gives you access to a Free OCI environment, including:
* 30-Days Free Trial with 300$ Credits for all OCI services
* Always Free Cloud Services: many free OCI services like AMD&ARM based Compute VMs, storage, 2 Autonomous DB, Load-Balancer, ...

This option requires a Credit Card during registration for account validation but it won't be used unless you activate your account to a Paid account after the 30-days.
It allows you to keep your JD Edwards environment and finalize the workshops labs at home.

### 2. Oracle OCI Workshop Tenant [https://ctf.solarpace.io/register](https://ctf.solarpace.io/register)

This option gives you limited access the Oracle OCI Tenant during the event and for the next 2 weeks - after this period, the deployed resources will be automatically removed.

#### Task 1. create your OCI account

Registration with automatic OCI provisioning is done using this URL: [https://ctf.solarpace.io](https://ctf.solarpace.io).
Please follow those recommendations for the registration:
  * User Name: enter a short username (first letter of First Name + last name like __gvoisin__)
  * email: your professional email address
  * password: this password will only be used for the CTF platform; a temporary OCI password will be sent by email during the account provisioning process
  * company: your Company Name
  * Registration Code: your trainer will give you the code during the event

 ![CTF registration](./images/ctf-registration2.png)

After validating your registration you should receive 3 emails:
1. a **Welcome to Oracle Cloud - Sign in and get started** email sent automatically by OCI
2. a **Successfully registered for Oracle JD Edwards Trial Edition & Orchestrator workshop - day Month 2o22**
3. a **Temporary Password for Oracle OCI Tenant oractdemeaoci** with the link, username (***jdeday-username***)and temporary password to connect to OCI

 ![email](./images/email.png)

Please use the third email:
  * click on the provided link 
  * this OCI tenant is configured for Direct Sign-in so, on the login page, click on the **Dropdown** arrow:

  ![oci-login-dropdown.png](./images/oci-login-dropdown.png)

  * enter your username (jdeday-username) + temporary password and press the **Sign In** button

  ![oci-login-signin.png](./images/oci-login-signin.png)

  * upon first connection, you are requested to create your definitive password; enter the previous and new password and click on **Save New Password**

![change-pwd.png](./images/change-pwd.png)

😃 Well Done!  You are now connected to the OCI tenant and are ready to start the first workshop.

Please go to the Lab 1 🚀  [https://bit.ly/JdeDayLab1](https://bit.ly/JdeDayLab1)

